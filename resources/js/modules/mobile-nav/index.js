import $$ from '@utilities/selectors'

function addEvent(element, evnt, funct) {
  if (element.attachEvent) {
   return element.attachEvent('on'+evnt, funct);
 } else {
   return element.addEventListener(evnt, funct, false);
 }
}

function closeMenu() {
  Object.keys($$.mobileNavToggle).forEach(function (key) {
    $$.mobileNavToggle[key].setAttribute('aria-expanded', 'false');
  });
  $$.mobileNav.classList.remove('menu-visible');
  $$.wrapper.style.transform = "translate3d(0, 0, 0)";
  $$.wrapper.style.opacity = "1";
  document.documentElement.removeEventListener('click', closeMenuOutsideClick);
}

function openMenu() {
  const formWidth = $$.mobileNav.firstChild.offsetWidth;

  event.stopPropagation()

  Object.keys($$.mobileNavToggle).forEach(function (key) {
    $$.mobileNavToggle[key].setAttribute('aria-expanded', 'true');
  });
  $$.mobileNav.classList.add('menu-visible');
  $$.wrapper.style.transform = "translate3d(-" + formWidth + "px, 0, -24px)";
  $$.wrapper.style.opacity = "0.618";
  const html = document.documentElement;
  addEvent(
      html,
      'click',
      closeMenuOutsideClick
  );
}

function closeMenuOutsideClick(event) {
  // get the event path
  const path = event.composedPath();
  // check if it has the menu
  if ( path.some( elem => elem.id === 'form-wrap' ) ) {
    return;
  } else {
    closeMenu();
  }
}

const MobileNav = function MobileNav() {

    Object.keys($$.mobileNavToggle).forEach(function (key) {
      $$.mobileNavToggle[key].setAttribute('aria-expanded', 'false')
      $$.mobileNavToggle[key].addEventListener('click', function () {

        this.classList.toggle('expanded')

        // set aria-expanded attribute on menu toggle button
        if ( this.getAttribute('aria-expanded') === 'false' || !this.getAttribute('aria-expanded') ) {

            openMenu();

        } else {

            closeMenu();

        }

    })
  })
}()

export default MobileNav
