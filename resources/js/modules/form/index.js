import $$ from '@utilities/selectors'

const HandleSubmit = function HandleSubmit(e) {

  $$.contactForm.addEventListener('submit', e => {
    console.log(e.defaultPrevented)
    e.preventDefault()
    console.log(e.defaultPrevented)

    let myForm = $$.contactForm;
    let formData = new FormData(myForm)

    fetch($$.contactForm.getAttribute('action'), {

      method: 'POST',
      headers: {
        "Accept": "application/x-www-form-urlencoded",
        "Content-Type": "application/x-www-form-urlencoded"
      },
      body: new URLSearchParams(formData).toString()

    }).then(
      (res) => console.log(res)
    ).catch(
      (error) => alert(error)
    )
  })
}

export default HandleSubmit
