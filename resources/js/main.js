
// Import local modules
import '@modules/mobile-nav'
import '@modules/lazyload'
import '@modules/form'
import Pristine from 'pristinejs'

import $$ from '@utilities/selectors'
var DOMReady = function(a,b,c){b=document,c='addEventListener';b[c]?b[c]('DOMContentLoaded',a):window.attachEvent('onload',a)}
DOMReady(function () {
  $$.body.classList.remove('preload');
});

let defaultConfig = {
  // class of the parent element where the error/success class is added
  classTo: 'row',
  errorClass: 'has-danger',
  successClass: 'has-success',
  // class of the parent element where error text element is appended
  errorTextParent: 'row',
  // type of element to create for the error text
  errorTextTag: 'div',
  // class of the error text element
  errorTextClass: 'text-help'
}
var pristine = new Pristine($$.contactForm, defaultConfig)
$$.contactForm.addEventListener('submit', e => {
  e.preventDefault()

  let myForm = $$.contactForm;
  let formData = new FormData(myForm)

  var valid = pristine.validate()

  fetch($$.contactForm.getAttribute('action'), {

    method: 'POST',
    headers: {
      "Accept": "application/x-www-form-urlencoded",
      "Content-Type": "application/x-www-form-urlencoded"
    },
    body: new URLSearchParams(formData).toString()

  }).then(
    (res) => {
      const form = document.querySelector('#contact')
      form.innerHTML = '<p class="lead">Thanks for reaching out.</p><p>Someone from our team will be in touch with you soon.</p>'
      form.classList.add('submitted')
    }
  ).catch(
    (error) => alert(error)
  )
})

function iOS() {
  return [
    'iPad Simulator',
    'iPhone Simulator',
    'iPod Simulator',
    'iPad',
    'iPhone',
    'iPod'
  ].includes(navigator.platform)
  || (navigator.userAgent.includes("Mac") && "ontouchend" in document)
}
if ( iOS() ) {
  $$.body.classList.add('ios');
}
